package fizzbuzz;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
 
/**
 * FizzBuzz tests
 * @author rribeiro
 */
public class FizzBuzzTest {
    /**
     * Test Fizz
     */
    @Test
    public void testFIZZ() {
        assertEquals(FizzBuzzConstants.FIZZ, FizzBuzz.generateMultipleOutput(3));
        assertEquals(FizzBuzzConstants.FIZZ, FizzBuzz.generateMultipleOutput(6));
        assertEquals(FizzBuzzConstants.FIZZ, FizzBuzz.generateMultipleOutput(6));
        assertEquals(FizzBuzzConstants.FIZZ, FizzBuzz.generateMultipleOutput(9));
        assertEquals(FizzBuzzConstants.FIZZ, FizzBuzz.generateMultipleOutput(12));
        assertEquals(FizzBuzzConstants.FIZZ, FizzBuzz.generateMultipleOutput(54));
        assertEquals(FizzBuzzConstants.FIZZ, FizzBuzz.generateMultipleOutput(72));
    }
    
    /**
     * Test Buzz
     */
    @Test
    public void testBUZZ() {
        assertEquals(FizzBuzzConstants.BUZZ, FizzBuzz.generateMultipleOutput(5));
        assertEquals(FizzBuzzConstants.BUZZ, FizzBuzz.generateMultipleOutput(10));
        assertEquals(FizzBuzzConstants.BUZZ, FizzBuzz.generateMultipleOutput(20));
        assertEquals(FizzBuzzConstants.BUZZ, FizzBuzz.generateMultipleOutput(40));
        assertEquals(FizzBuzzConstants.BUZZ, FizzBuzz.generateMultipleOutput(55));
        assertEquals(FizzBuzzConstants.BUZZ, FizzBuzz.generateMultipleOutput(70));
        assertEquals(FizzBuzzConstants.BUZZ, FizzBuzz.generateMultipleOutput(80));
    }
    
    /**
     * Test FizzBuzz
     */
    @Test
    public void testFIZZBUZZ() {
        assertEquals(FizzBuzzConstants.FIZZBUZZ, FizzBuzz.generateMultipleOutput(15));
        assertEquals(FizzBuzzConstants.FIZZBUZZ, FizzBuzz.generateMultipleOutput(30));
        assertEquals(FizzBuzzConstants.FIZZBUZZ, FizzBuzz.generateMultipleOutput(45));
        assertEquals(FizzBuzzConstants.FIZZBUZZ, FizzBuzz.generateMultipleOutput(60));
        assertEquals(FizzBuzzConstants.FIZZBUZZ, FizzBuzz.generateMultipleOutput(75));
        assertEquals(FizzBuzzConstants.FIZZBUZZ, FizzBuzz.generateMultipleOutput(90));
    }
    
    /**
     * Test other numbers
     */
    @Test
    public void testOtherNumbers() {
        assertEquals(String.valueOf(1), FizzBuzz.generateMultipleOutput(1));
        assertEquals(String.valueOf(2), FizzBuzz.generateMultipleOutput(2));
        assertEquals(String.valueOf(4), FizzBuzz.generateMultipleOutput(4));
        assertEquals(String.valueOf(37), FizzBuzz.generateMultipleOutput(37));
        assertEquals(String.valueOf(52), FizzBuzz.generateMultipleOutput(52));
        assertEquals(String.valueOf(73), FizzBuzz.generateMultipleOutput(73));
    }
}
