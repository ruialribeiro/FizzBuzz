package fizzbuzz;

/**
 * Main class to run the FizzBuzz program
 * @author rribeiro
 */
public class FizzBuzz {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        runFizzBuzz(100);
    }
    
    /**
     * Loop from 1 until n and generate the output
     * @param maxNumber max number
     */
    public static void runFizzBuzz(int maxNumber) {
        for(int i = 1; i <= maxNumber; i++){
            String output = generateMultipleOutput(i);
            System.out.println(output);
        }    
    }
    
    /**
     * Generate ouput based on the number passed
     * @param number
     * @return FizzBuzz if multiple of 3 and 5
     *         Fizz if multiple of 3
     *         Buzz if multiple of 5
     *         number otherwise
     */
    public static String generateMultipleOutput(int number){
        String result = String.valueOf(number);
        
        if(isMultipleOf3And5(number)){
            result = FizzBuzzConstants.FIZZBUZZ;
        }
        else if(isMultipleOf3(number)){
            result = FizzBuzzConstants.FIZZ;
        }
        else if(isMutipleOf5(number)){
            result = FizzBuzzConstants.BUZZ;
        }
        return result;
    }
    
    /**
     * Check if the number is a multiple of 3
     * 
     * @param number
     * @return true if the number is multiple of 3 otherwise false
     */
    private static boolean isMultipleOf3(int number){
        return number%3 == 0;
    }
    
    /**
     * Check if the number is a multiple of 5
     * 
     * @param number
     * @return true if the number is multiple of 5 otherwise false
     */
    private static boolean isMutipleOf5(int number){
        return number%5 == 0;
    }
    
    /**
     * Check if the number is a multiple of 3 and 5
     * 
     * @param number
     * @return true if the number is multiple of 3 and 5 otherwise false
     */
    private static boolean isMultipleOf3And5(int number){
        return isMultipleOf3(number) && isMutipleOf5(number);
    } 
}
