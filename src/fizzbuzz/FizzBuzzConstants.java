package fizzbuzz;

/**
 * FizzBuzz constants
 * @author rribeiro
 */
public class FizzBuzzConstants {
    protected static final String FIZZ = "Fizz";
    protected static final String BUZZ = "Buzz";
    protected static final String FIZZBUZZ = "FizzBuzz";
}
